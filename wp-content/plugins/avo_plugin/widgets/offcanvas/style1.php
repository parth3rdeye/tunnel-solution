<?php
	use Elementor\Icons_Manager;
?>

		<div class="header-offcanvas-icon hidden-xs hidden-sm">
			<a class="offcanvas"  href="#">
				<?php Icons_Manager::render_icon( $settings['avo_offcanvas_icons'], [ 'aria-hidden' => 'true' ] );?>
			</a>
		</div>