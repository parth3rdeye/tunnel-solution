<?php 

use Elementor\Icons_Manager;

?>
    <div class="button style-3"> 
        <a target="_blank" class="purchase-theme btn vc_hidden-xs visible" href="<?php echo esc_url( $settings['link']['url']); ?>">
            <i class="icon">
                <img loading="lazy" src="https://colabrio.ams3.cdn.digitaloceanspaces.com/stockie/st__envato.svg" alt="envato icon">
            </i><?php echo  $settings['btn_text']; ?>
        </a>
    </div>