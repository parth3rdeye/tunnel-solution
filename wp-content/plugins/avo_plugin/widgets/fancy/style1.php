                    <div class="exp-img wow fadeInUp" data-wow-delay=".3s">
                        <div class="img bg-img wow imago" style="background-image:url(<?php echo esc_url ($settings['image_bg']['url']); ?>);">
                            <div class="since custom-font">
                                <span><?php echo esc_attr($settings['text1']); ?></span>
                                <span><?php echo esc_attr($settings['text2']); ?></span>
                            </div>
                            <div class="years custom-font">
                                <h2 style="background-image:url(<?php echo esc_url ($settings['image_bg']['url']); ?>);">
                                    <?php echo esc_attr($settings['title2']); ?>   
                                </h2>
                                <h5><?php echo esc_attr($settings['title1']); ?></h5>
                            </div>
                        </div>
                    </div>