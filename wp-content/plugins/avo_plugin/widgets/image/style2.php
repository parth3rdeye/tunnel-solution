<div class="avo-image style-2 <?php if ( ! empty( $settings['title'] ) ) { echo 'avo-tooltip';}?>  ">
	    <div class="img2 imago animate" data-tooltip-tit="<?php echo  $settings['title']; ?>" data-tooltip-sub="<?php echo  $settings['subtitle']; ?>">
	        <img src="<?php echo esc_url ( $settings['image']['url']); ?>" alt="" class="imago wow">
	    </div> 
	    <div class="div-tooltip-tit"><?php echo  $settings['title']; ?></div>
</div>