

<div class="avo-header style-5 <?php echo 'text-'.$settings['align_title'];?>">
    <div class="container">
        <div class="sec-head custom-font">
            <h6 class="custom-font"><?php echo  $settings['title_1']; ?></h6>
            <h3 class="custom-font"><?php echo  $settings['title_2']; ?></h3>
            <span class="tbg <?php echo 'text-'.$settings['align_header'];?>"><?php echo  $settings['title']; ?></span>
        </div>
    </div>
</div>