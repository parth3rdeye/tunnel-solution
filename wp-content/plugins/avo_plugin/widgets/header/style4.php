

<div class="avo-header style-4">
    <div class="pages-header bg-img valign parallaxie" data-background="<?php echo esc_url ($settings['image']['url']); ?>" data-overlay-dark="5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cont text-center">
                        <h1><?php echo  $settings['title']; ?></h1>
                        <div class="path">
                            <?php echo avo_breadcrumbs(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>