

<div class="avo-header style-3 <?php echo $avo_rtl;?>">
    <div class="works-header fixed-slider valign">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-9 col-md-11 static">
                    <div class="capt mt-50">
                        <div class="<?php echo $avo_parlx;?>">
                            <h2 class="custom-font"><span><?php echo  $settings['title_1']; ?></span><?php echo  $settings['title_2']; ?></h2>
                            <p><?php echo  $settings['text']; ?></p>
                        </div>

                        <div class="bactxt custom-font valign">
                            <span class="full-width"><?php echo  $settings['title']; ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>