<div class="avo-progress-bar style-1">
	<div class="skills-box full-width">
	    <div class="skill-item">
	        <h6 class="custom-font"><?php echo  $settings['title']; ?></h6>
	        <div class="skill-progress">
	            <div class="progres custom-font" data-value="<?php echo esc_attr( $settings['percentage']['size'] );?>%"></div>
	        </div>
	    </div>
	</div>
</div>
