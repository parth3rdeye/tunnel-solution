<?php

//oneclick importer
function ocdi_import_files() {
	return array(
		array(
			'import_file_name'           => '1-Digital Agency',
			'import_file_url'            => plugins_url( '/demo-data/demo1/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo1/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo1/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo1/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo1/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com',
		),
		array(
			'import_file_name'           => '2-Creative Studio',
			'import_file_url'            => plugins_url( '/demo-data/demo2/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo2/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo2/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo2/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo2/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo2',
		),
		array(
			'import_file_name'           => '3-Business Startup',
			'import_file_url'            => plugins_url( '/demo-data/demo3/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo3/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo3/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo3/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo3/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo3',
		),
		array(
			'import_file_name'           => '4-One page',
			'import_file_url'            => plugins_url( '/demo-data/demo4/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo4/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo4/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo4/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo4/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo4',
		),
		array(
			'import_file_name'           => '5-Personal',
			'import_file_url'            => plugins_url( '/demo-data/demo5/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo5/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo5/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo5/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo5/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo5',
		),			
		array(
			'import_file_name'           => '6-Shop',
			'import_file_url'            => plugins_url( '/demo-data/demo6/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo6/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo6/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo6/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo6/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo6',
		),	
		array(
			'import_file_name'           => '7-Full screen',
			'import_file_url'            => plugins_url( '/demo-data/demo7/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo7/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo7/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo7/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo7/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo7',
		),			
		array(
			'import_file_name'           => '8-creative carousal',
			'import_file_url'            => plugins_url( '/demo-data/demo8/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo8/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo8/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo8/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo8/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo8',
		),	
		array(
			'import_file_name'           => '9-Columns Slider',
			'import_file_url'            => plugins_url( '/demo-data/demo9/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo9/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo9/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo9/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo9/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo9',
		),			
		array(
			'import_file_name'           => '10-Radius slider',
			'import_file_url'            => plugins_url( '/demo-data/demo10/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo10/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo10/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo10/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo10/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo10',
		),	
		array(
			'import_file_name'           => '11-Video show',
			'import_file_url'            => plugins_url( '/demo-data/demo11/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo11/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo11/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo11/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo11/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo11',
		),
		array(
			'import_file_name'           => '12-Maintenance',
			'import_file_url'            => plugins_url( '/demo-data/demo12/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo12/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo12/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo12/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo12/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo12',
		),
		
		array(
			'import_file_name'           => '13-All Inner pages',
			'import_file_url'            => plugins_url( '/demo-data/demo13/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo13/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo13/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo13/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo13/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo13',
		),			
		array(
			'import_file_name'           => '14-All Elements',
			'import_file_url'            => plugins_url( '/demo-data/demo14/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo14/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo14/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo14/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo14/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo14',
		),	
		array(
			'import_file_name'           => '15-All Blog layouts',
			'import_file_url'            => plugins_url( '/demo-data/demo15/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo15/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo15/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo15/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo15/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo15',
		),
				
		array(
			'import_file_name'           => '16-corporate',
			'import_file_url'            => plugins_url( '/demo-data/demo16/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo16/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo16/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo16/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo16/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo16',
		),	
		array(
			'import_file_name'           => '17-Portfolios',
			'import_file_url'            => plugins_url( '/demo-data/demo17/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo17/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo17/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo17/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo17/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo17',
		),			
		array(
			'import_file_name'           => '18-RTL',
			'import_file_url'            => plugins_url( '/demo-data/demo18/content.xml' , __FILE__ ),
			'import_widget_file_url'     => plugins_url( '/demo-data/demo18/widgets.wie' , __FILE__ ),
			'import_customizer_file_url'  => plugins_url( '/demo-data/demo18/customizer.dat' , __FILE__ ),
			'import_redux'           => array(
				array(
					'file_url'   => plugins_url( '/demo-data/demo18/redux.json' , __FILE__ ),
					'option_name' => 'avo_theme_setting',
				),
			),
			'import_preview_image_url'   => plugins_url( '/demo-data/demo18/preview.jpg' , __FILE__ ),
			'import_notice'                => __( '<p>To prevent any error, please use the clean wordpress site to import the demo data. </p><p>Or you can use this plugin 
			<a href="https://wordpress.org/plugins/wordpress-database-reset/" target="_blank">WordPress Database Reset</a> to reset/clear the database first.</p><p>After you import this demo, you will have to setup the elementor page builder .</p>', 'avo_plg' ),
			'preview_url'                => 'https://avo.smartinnovates.com/demo18',
		)
	);
}
add_filter( 'pt-ocdi/import_files', 'ocdi_import_files' );

/*-----------automatically assign "Front page", "Posts page" and menu locations ---------------------------*/



function ocdi_after_import( $selected_import ) {

	// Assign menus to their locations.
	$main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );

	set_theme_mod( 'nav_menu_locations', array(
			'primary_menu' => $main_menu->term_id, // replace 'main-menu' here with the menu location identifier from register_nav_menu() function
		)
	);

	if ( '1-Digital Agency' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home dark' );
	}
	elseif ( '2-Creative Studio' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home02 dark' );
	}
	elseif ( '3-Business Startup' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home03 dark' );
	}
	elseif ( '4-One page' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home04 dark' );
	}
	elseif ( '5-Personal' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home05 dark' );
	}
	elseif ( '6-Shop' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home06 dark' );
	}
	elseif ( '7-Full screen' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home07 dark' );
	}
	elseif ( '8-creative carousal' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home08 dark' );
	}
	elseif ( '9-Columns Slider' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home09 dark' );
	}
	elseif ( '10-Radius slider' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home10 dark' );
	}
	elseif ( '11-Video show' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home11 dark' );
	}
	elseif ( '12-Maintenance' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home12 dark' );
	}
	/*
	elseif ( '13-Personal' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home13 dark' );
	}
	elseif ( '14-Personal' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home14 dark' );
	}
	elseif ( '15-Personal' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home15 dark' );
	}
	*/
	elseif ( '16-corporate' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home16 dark' );
	}
	elseif ( '17-Portfolios' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home17 dark' );
	}
	elseif ( '18-RTL' === $selected_import['import_file_name'] ) {
		// Assign front page.
		$front_page_id = get_page_by_title( 'Home18 dark' );
	}
	update_option( 'show_on_front', 'page' );
	update_option( 'page_on_front', $front_page_id->ID );
	update_option( 'elementor_disable_color_schemes', 'yes' ); 
	update_option( 'elementor_disable_typography_schemes', 'yes' ); 
	update_option( 'elementor_load_fa4_shim', 'yes' ); 
	update_option( 'elementor_container_width', 1200 );
	$cpt_support = [ 'page', 'post','product','portfolio','footer','header','sidepanel' ];
	update_option( 'elementor_cpt_support', $cpt_support ); //update 'Costom post type'
}
add_action( 'pt-ocdi/after_import', 'ocdi_after_import' );

/*------------------disable the ProteusThemes branding notice -----------------------------------*/

add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

/*------------------Adding notes -----------------------------------*/

function ocdi_plugin_intro_text( $default_text ) {
	$default_text .= '<div class="ocdi__intro-text"><strong>Server requirements:</strong></div>';
	$default_text .= '<div class="ocdi__intro-text"><ul>
		<li>max_execution_time 3000</li>
		<li>memory_limit 128M</li>
		<li>post_max_size 64M</li>
		<li>upload_max_filesize 64M</li>
		<li>max_input_time 180</li>
	</ul></div><hr>';

	return $default_text;
}
add_filter( 'pt-ocdi/plugin_intro_text', 'ocdi_plugin_intro_text' );
